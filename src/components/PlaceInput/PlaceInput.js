import React, { Component } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';

class PlaceInput extends Component {

	state = {
	    placeName: ''
	  }

	  placeNameChangedHandeler = val => {
	    this.setState({
	      placeName: val
	    });
	  };

	  placeSubmitHandler = () => {
	    if (this.state.placeName.trim() === ""){
	      return;
	    }
	    this.props.onPlaceAdded(this.state.placeName);
	  };

	render(){
		return (
			<View style={styles.inputContainer}>
		        <TextInput
		            placeholder="Awesome place"
		            clearButtonMode="always"
		            style={styles.placeInput}
		            value={this.state.placeName}
		            onChangeText={this.placeNameChangedHandeler}
		        />
		        <Button title="Add" 
		        	style={styles.placeButton} 
		        	onPress={this.placeSubmitHandler} 
		        />
	        </View>
		);
	}
}

const styles = StyleSheet.create({
	inputContainer: {
	    //flex: 1,
	    width: "100%",
	    flexDirection: "row",
	    justifyContent: "space-between",
	    alignItems: "center"
	  },
	  placeInput: {
	    borderWidth: 0.3,
	    width: "70%"
	  },
	  placeButton: {
	    width: "30%"
	  }
});

export default PlaceInput;