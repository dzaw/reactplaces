import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import PlaceInput from './src/components/PlaceInput/PlaceInput';
import PlaceList from './src/components/PlaceList/PlaceList';
//import placeImage from './src/assets/react-icon.png';
import PlaceDetail from './src/components/PlaceDetail/PlaceDetail';

type Props = {};
export default class App extends Component<Props> {

  state = {
    places: [],
    selectedPlace: null
  }

  placeAddedHandler = placeName => {
    this.setState(prevState => {
      return {
        places: prevState.places.concat({
          key: Math.random(), 
          name: placeName,
          image: {
            uri: "https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
          }
      })
    };
  });
  };

  placeDeteledHandler = () => {
    this.setState(prevState => {
      return {
        places: prevState.places.filter(place => {
          return place.key !== prevState.selectedPlace.key;
        }),
        selectedPlace: null
      }
    });
  }

  modalClosedHandler = () => {
    this.setState({
      selectedPlace: null
    });
  }

  placeSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedPlace: prevState.places.find(place => {
          return place.key === key;
        })
      }
    });
  };

  render() {
    
    return (
      <View style={styles.container}>
      <Text style={styles.welcome}>React Native!</Text>
        <PlaceDetail selectedPlace={this.state.selectedPlace} onItemDeleted={this.placeDeteledHandler} onModalClosed={this.modalClosedHandler}/>
        <PlaceInput onPlaceAdded={this.placeAddedHandler}  />
        <PlaceList places={this.state.places} 
        onItemSelected={this.placeSelectedHandler} />
        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  
  
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
